# Daily Philosophy Bot

A simple bot that grabs a random podcast from the Freedomain API and posts it to your Telegram and Discord server



## Configuration

- Obtain a discord webhook URL by following the guide: [Intro to Webhooks](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks)

- Obtain your [telegram bot token](https://core.telegram.org/bots#6-botfather) and [channel ID](https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id)


