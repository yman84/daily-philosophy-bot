#!/usr/bin/python

import random
import requests
import time
from datetime import date

# To-DO
# append posted podcasts to a text file so there are no repeats
# Read from text file to find which podcasts have already been posted so no repeats


PodcastNumber = ""

# if you want to post a specific podcast, input it here. 
# otherwise comment out this line
# PodcastNumber = 2675

PostType = 'Testing'
# PostType = 'Freedomain'

if PostType == 'Testing':
    DiscordWebhookURL = ''
    TelegramBotToken = ''
    TelegramChannelChatID = '' 


if PostType == 'Freedomain':
    TelegramBotToken = ''
    TelegramChannelChatID = ''
    DiscordWebhookURL = ''
    pass


# Remove commonly repeating texts
def clean_description(description):
    SplitAtTextList = 'Freedomain Radio is 100% funded by viewers like you.','Your support is essential to Freedomain Radio','▶️ 1. Donate: ','️ Donate Now: www.freedomain.com/donate','▶️ Donate Now:','Free Documentaries:',

    for splitText in SplitAtTextList: 

        if splitText in description:
            description = description.split(splitText)[0]
            
    description = description.strip() # remove trailing white spaces
    return description


# move backwards within the description until you hit your desired description length
# and reach a white space so words don't get cut in half
def trim_description_length(text, length):
    lenMinusOne = length
    lenMinusTwo = length - 1

    while True:
        textAtPosition = text[lenMinusTwo:lenMinusOne]
        # print(textAtPosition)
        if textAtPosition == ' ':
            text = text[0:lenMinusOne]
            # print("final: " + textAtPosition)
            return text
        lenMinusOne -= 1
        lenMinusTwo -= 1
    

def post_to_social_media(podcastnumber, socialmedianetworks):

    url = f"https://fdrpodcasts.com/api/v2/podcasts/?num={PodcastNumber}&includeTagNames=true"

    PodcastHyperlink = 'https://fdrpodcasts.com/' + str(PodcastNumber)

    data = (requests.get(url)).json()


    try:
        title = data['podcasts'][0]['title']
    except:
        return "PodcastRemoved"

    # initialize the variables with the podcast hyperlinks
    discordURLsBody = "[Podcast](<" + PodcastHyperlink + ">) \\| "
    TelegramURLsBody = f'<a href="{PodcastHyperlink}">Podcast</a>' + " | "


    # Podcast date is down to the second, pull out just the year,month,day
    podcast_date = data['podcasts'][0]['date']
    podcast_date = podcast_date[0:10]
    podcast_date = podcast_date.replace("-", "/")


    description = data['podcasts'][0]['description']

    # clean description
    Discorddescription = clean_description(description)
    telegramDescription = Discorddescription


    discordURLsBody = "[Podcast](<" + PodcastHyperlink + ">) \\| "
    TelegramURLsBody = f'<a href="{PodcastHyperlink}">Podcast</a>' + " | "

    # Thanks James!
    if len(data['podcasts']) > 0 and 'urls' in data['podcasts'][0]:
        urls = data['podcasts'][0]['urls']

        AudioURL = urls.get('audio')
        discordURLsBody += "[Audio](<" + AudioURL  + ">) \\| "
        TelegramURLsBody += f'<a href="{AudioURL}">Audio</a>' + " | "

        BitchuteURL = urls.get('bitchute')
        discordURLsBody += "[BitChute](<" + BitchuteURL + ">) \\| " if BitchuteURL else ""
        TelegramURLsBody += f'<a href="{BitchuteURL}">Rumble</a>' + " | " if BitchuteURL else ""

        RumbleURL = urls.get('rumble')
        discordURLsBody += "[Rumble](<" + RumbleURL + ">) \\| " if RumbleURL else ""
        TelegramURLsBody += f'<a href="{RumbleURL}">Rumble</a>' + " | " if RumbleURL else ""

        LBRYaudioURL = urls.get('lbry-audio')
        discordURLsBody += "[HQ Audio](<" + LBRYAudioURL + ">) \\| " if LBRYaudioURL else ""
        TelegramURLsBody += f'<a href="{LBRYAudioURL}">Odysee</a>' + " | " if LBRYaudioURL else ""

        LbryVideoURL = urls.get('lbry')
        discordURLsBody += "[Odysee](<" + LbryVideoURL + ">) \\| " if LbryVideoURL else ""
        TelegramURLsBody += f'<a href="{LbryVideoURL}">Odysee</a>' + " | " if LbryVideoURL else ""

        DailyMotionUrl = urls.get('dailymotion')
        discordURLsBody += "[DailyMotion](<" + DailyMotionUrl + ">) \\| " if DailyMotionUrl else ""
        TelegramURLsBody += f'<a href="{DailyMotionUrl}">DailyMotion</a>' + " | " if DailyMotionUrl else ""

        BrighteonURL = urls.get('brighteon')
        discordURLsBody += "[Brighteon](<" + BrighteonURL + ">) \\| " if BrighteonURL else ""
        TelegramURLsBody += f'<a href="{BrighteonURL}">Brighteon</a>' + " | " if BrighteonURL else ""

        StreamanityUrl = urls.get('streamanity')
        discordURLsBody += "[Streamanity](<" + StreamanityUrl + ">) \\| "if StreamanityUrl else ""
        TelegramURLsBody += f'<a href="{StreamanityUrl}">Streamanity</a>' + " | " if StreamanityUrl else ""

        thumbnail = urls.get('thumbnail')

    # subscribe to podcast feed hyperlink
    discordURLsBody += "[Subscribe to Podcast Feed](<" + "https://freedomainplaylists.com/wp-content/DailyPhilosophy/DailyPhilosophy.xml" + ">) \\| "
    TelegramURLsBody += f'<a href="https://freedomainplaylists.com/wp-content/DailyPhilosophy/DailyPhilosophy.xml">Subscribe to Daily Podcast Feed</a>' + " | "


    numOfcharsLeftDiscordWbhkBody = 2000 - len("**" + title + "**" + " " + f"({PodcastNumber})" + "\n" + "\n" + discordURLsBody) - 50
    numOfcharsLeftTelegramAPIBody = 2000 - len("**" + title + "**" + " " + f"({PodcastNumber})" + "\n" + "\n" + TelegramURLsBody) - 50
    # numOfcharsLeftTelegramAPIBody = 4096 - len("**" + title + "**" + " " + f"({PodcastNumber})" + "\n" + "\n" + TelegramURLsBody) - 50
    # removed for now  because messages were wayyy too long on mobile

    # if description is too long for message, then trim it down to fit
    if len(Discorddescription) > numOfcharsLeftDiscordWbhkBody:
        Discorddescription = trim_description_length(Discorddescription, numOfcharsLeftDiscordWbhkBody) + " " + "[[...]](<https://fdrpodcasts.com/" + str(PodcastNumber) + ">)" 
        
    if len(telegramDescription) > numOfcharsLeftTelegramAPIBody:
        telegramDescription = trim_description_length(telegramDescription, numOfcharsLeftDiscordWbhkBody) + " " + f'<a href="{PodcastHyperlink}">[...]</a>'

    # switch out the thumbnail url with a temporary different one because telegram cached the original I linked to and won't update their cache
    if 'FreedomainLogo.png' in thumbnail:
        thumbnail = 'https://cdn.discordapp.com/attachments/799495556312334336/917629667387785267/FreedomainLogo1.png'

    discordMessage = "**" + title + "**" + " " + f"[{PodcastNumber}] " + f"({podcast_date})" + "\n" + str(Discorddescription) + "\n" + discordURLsBody
    telegram_message = f'<a href="{thumbnail}">&#8204;</a>' + f"<b>{title}</b>" + f" [{PodcastNumber}]" + f" ({podcast_date})" + "\n" + str(telegramDescription) + "\n" + TelegramURLsBody 

    # post to discord
    response = requests.post(DiscordWebhookURL,json={"content": discordMessage, "embeds": [{"image": {"url": f"{thumbnail}"}}]}).text
    print('discord response: ' + str(response))

    # post to telegram
    response = requests.post(f'https://api.telegram.org/bot{TelegramBotToken}/sendMessage',json={"chat_id": TelegramChannelChatID, "text": telegram_message, "parse_mode": "HTML"})
    print('Telegram response: ' + str(response))

    return


if PostType == 'Freedomain' and PodcastNumber != '':

    post_to_social_media(PodcastNumber, "discordtelegram")
    
    # Append to file for the podcast feed to read from
    date = date.today().today.strftime("%Y/%m/%d")
    with open("/home/yuriy/Syncthing/Freedomain/RandomPodcast/PostedPodcasts_Freedomain", 'a') as file1: 
        file1.write("\n" + str(PodcastNumber) + "-" + str(date))


if PostType == 'Testing':
    if PodcastNumber == "":
        # Get a handful of random podcasts in your testing server to choose from
        for x in range(1, 20):
            PodcastNumber = random.randint(0, 4936)
            post_to_social_media(PodcastNumber, "discordtelegram")
            time.sleep(5)
    else:
            post_to_social_media(PodcastNumber, "discordtelegram")


